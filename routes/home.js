const express = require('express');
const router = express.Router();
const client = require('../common/ServiceClient');
const MsgJsonHelper = require('../common/MsgJsonHelper');
const QueryModel = require('../common/QueryModel');
const MemoryCondition = require('../common/MemoryCondition');
const { MType, MLogic, MOperator, DataAccess, OperationEnum, Public } = require('msg-dataaccess-base');
const Routebase = require('./route.base');
const ZK_USERINFO = require('../model/ZK_USERINFO');

const ds = new DataAccess();

router.all('*', (req, res, next) => {
	if (Routebase.IsLogin(req, res)) {
		if (Routebase.IsPermit(req, res, '00001')) {
			next();
		}
	}
});

router.get('/menutree', (req, res) => {
	GetMenuTree(req, res);
});

router.get('/userlist', (req, res) => {
	BindUserList(req, res);
});

router.get('/rolelist', (req, res) => {
	BindRoleList(req, res);
});

router.get('/navtree', (req, res) => {
	BindNavtree(req, res);
});

router.post('/usersetup', (req, res) => {
	UpdateUserSetup(req, res);
});

/**
 * 获取菜单树
 * @param {*} req
 * @param {*} res
 */
function GetMenuTree(req, res) {
	let keys = [];
	let condition = [];
	req.UserPermit.forEach(item => {
		keys.push(item.ZK_PERMITID);
	});
	condition.push(
		new MemoryCondition({
			Field: 'ZK_ID',
			Logic: MLogic.And,
			Operator: MOperator.In,
			Type: MType.Mstring,
			value: keys.join(','),
		})
	);
	client
		.Query(QueryModel.ZK_PERMITINFO, condition, null, 0, 0, false, null)
		.then(m => {
			if (m.result.length === 0) {
				res.json(MsgJsonHelper.DebugJson('暂无权限数据'));
			} else {
				res.json(MsgJsonHelper.DefaultJson(m.result, true, ''));
			}
		})
		.catch(err => {
			res.json(MsgJsonHelper.DebugJson('GetMenuTree接口请求异常'));
		});
}

/**
 * 获取用户下拉
 * @param {*} req
 * @param {*} res
 */
function BindUserList(req, res) {
	res.json(MsgJsonHelper.DebugJson('暂未开发用户下拉列表'));
}

/**
 * 获取角色下拉
 * @param {*} req
 * @param {*} res
 */
function BindRoleList(req, res) {
	Routebase.ValidRole(req.UserInfo.ZK_ROLE)
		.then(roles => {
			if (roles.length === 0) {
				res.json(MsgJsonHelper.DebugJson('暂无您能获取的权限信息'));
			} else {
				res.json(MsgJsonHelper.DefaultJson(roles, true, ''));
			}
		})
		.catch(err => {
			res.json(MsgJsonHelper.DebugJson('BindRoleList接口请求异常'));
		});
}

/**
 * 获取文章目录树（导航用）
 * @param {*} req
 * @param {*} res
 */
function BindNavtree(req, res) {
	let condition = [];
	condition.push(
		new MemoryCondition({
			Field: 'EB_ISDELETE',
			Logic: MLogic.And,
			Operator: MOperator.In,
			Type: MType.Mstring,
			value: '0',
		})
	);
	client
		.Query(QueryModel.BindNavtree, condition, null, 0, 0, false, null)
		.then(m => {
			if (m.result.length === 0) {
				res.json(MsgJsonHelper.DebugJson('暂无数据'));
			} else {
				res.json(MsgJsonHelper.DefaultJson(m.result, true, ''));
			}
		})
		.catch(err => {
			res.json(MsgJsonHelper.DebugJson('BindNavtree接口请求异常'));
		});
}

/**
 * 更新用户信息
 * @param {*} req
 * @param {*} res
 */
function UpdateUserSetup(req, res) {
	let user = new ZK_USERINFO();
	user.ZK_ID = req.UserInfo.ZK_ID;
	user.EB_LASTMODIFY_DATETIME = new Date();
	user.EB_LASTMODIFYBY = req.UserInfo.ZK_ID;
	user.ZK_DEPARTMENT = req.body['ZK_DEPARTMENT'] || '';
	user.ZK_EMAIL = req.body['ZK_EMAIL'] || '';
	user.ZK_HEAD_PORTRAIT = req.body['ZK_HEAD_PORTRAIT'] || '';
	user.ZK_NAME = req.body['ZK_NAME'] || '';
	user.ZK_PASSWORD = req.body['ZK_PASSWORD_MD5'] || undefined; // 密码为空时跳过更新密码
	user.ZK_PHONE = req.body['ZK_PHONE'] || '';
	user.ZK_REMARK = req.body['ZK_REMARK'] || '';
	user.ZK_ROLE = req.body['ZK_ROLE'] || '';
	user.ZK_SEX = req.body['ZK_SEX'] || '';

	ds.TransRunQuery(Public.OperationSQLParams(user, OperationEnum.UpdateNoCheck))
		.then(flag => {
			res.json(MsgJsonHelper.DefaultJson(null, flag, flag ? '保存成功' : '修改失败，请检查数据后重新提交'));
		})
		.catch(err => {
			res.json(MsgJsonHelper.DebugJson('UpdateUserSetup接口调用失败'));
		});
}

module.exports = router;
